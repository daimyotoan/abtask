package vn.digitalstep.abtask.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.dialog.WSConfig;
import vn.digitalstep.abtask.model.User;
import vn.digitalstep.abtask.util.FileUtils;
import vn.digitalstep.abtask.util.PermissionUtil;
import vn.digitalstep.abtask.util.SharedPreferenceUtils;
import vn.digitalstep.abtask.util.Util;

/**
 * Created by LIO1HC on 11/26/2017.
 */

public class StartActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private Toolbar toolbar;
    //google sign-in
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleSignInpApiClient;
    private static final String TAG = "StartActivity";
    private static final int RC_SIGN_IN = 9001;

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference dataRef;

    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_start);
        findViewById(R.id.signin).setOnClickListener(onClickListener);
        initGoogleAPIs();
        initFacebookLogin();
        initFirebase();
        initFolder();
        initPermission();

        if (mFirebaseUser == null) {
            SharedPreferenceUtils.setLogin(this, false);
            findViewById(R.id.signin).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_fb_login).setVisibility(View.VISIBLE);
        }

        if (SharedPreferenceUtils.isLoggedIn(this)) {
            Intent intent = new Intent(StartActivity.this, MainActivity.class);
            finish();
            startActivity(intent);
        }
    }

    private void initPermission() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION};
        ActivityCompat.requestPermissions(this, permissions, PermissionUtil.PERMISSIONS_REQUEST_ALL);
        boolean res = PermissionUtil.getPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE,
                PermissionUtil.PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL);
        boolean res1 = PermissionUtil.getPermission(this, Manifest.permission.ACCESS_FINE_LOCATION,
                PermissionUtil.PERMISSIONS_LOCATION);
        boolean res2 = PermissionUtil.getPermission(this, Manifest.permission.CAMERA,
                PermissionUtil.PERMISSIONS_REQUEST_CAMERA);

    }

    private void initFacebookLogin() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.btn_fb_login);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });
    }

    private void initFolder() {
        String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/abtask";
        FileUtils.createFolder(PATH);

        PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/abtask/photo";
        FileUtils.createFolder(PATH);
    }

    private void initGoogleAPIs() {
        //initialize google sign-in api
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestId()
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestProfile()
                .requestEmail()
                .build();
        mGoogleSignInpApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initFirebase() {
        // Initialize FirebaseAuth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        dataRef = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed
                Log.e(TAG, "Google Sign In failed.");
            }
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInpApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGooogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(StartActivity.this, task.getException().toString(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            handleSignIn();
                        }
                    }
                });
    }

    private void handleSignIn() {
        // Initialize FirebaseUser
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, show upper pane and hide lower pane
        } else {
            // Signed in, save flag to Sharepreference
            try {
                Util.hideLoading();
            } catch (Exception ex) {
                Log.e("HIDE_LOADING", ex.getMessage());
            }
            SharedPreferenceUtils.setLogin(this, true);

            Util.showLoading(this);
            dataRef.child(WSConfig.PATH_USER).child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {        //user not in database
                        User user = new User();
                        user.fullName = mFirebaseUser.getDisplayName();
                        user.email = mFirebaseUser.getEmail();
                        user.uid = mFirebaseUser.getUid();
                        user.timestamp = new Date().getTime();
                        if (mFirebaseUser.getPhotoUrl() != null)
                            user.urlPhoto = mFirebaseUser.getPhotoUrl().toString();

                        dataRef.child(WSConfig.PATH_USER).child(mFirebaseUser.getUid()).setValue(user, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                                finish();
                                startActivity(intent);
                            }
                        });
                    } else {
                        Intent intent = new Intent(StartActivity.this, MainActivity.class);
                        finish();
                        startActivity(intent);
                    }
                    Util.hideLoading();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Util.hideLoading();
                }
            });
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(StartActivity.this, task.getException().toString(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            handleSignIn();
                        }
                    }
                });
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.signin:
                    signIn();
                    break;
                default:
                    break;
            }
        }
    };

}
