package vn.digitalstep.abtask.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by LIO1HC on 11/27/2017.
 */

public class Project implements Comparable<Project>, Serializable {
    public Project() {
    }

    public String key = "";
    public String name = "";
    public long timestamp = 0;
    public String description = "";
    public String owner = "";
    public String urlPhoto = "";
    public int status = 1;      //0:inactive, 1:active

    @Override
    public int compareTo(@NonNull Project project) {
        if (timestamp == 0)
            return 0;

        Date a = new Date();
        a.setTime(timestamp);
        Date b = new Date();
        b.setTime(project.timestamp);
        if (a.before(b))
            return -1;
        else if (a.after(b)) // it's equals
            return 1;
        else
            return 0;
    }
}
