package vn.digitalstep.abtask.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by LIO1HC on 11/27/2017.
 */

public class Item implements Comparable<Item>, Serializable {
    public Item() {
    }

    public String key = "";
    public String name = "";
    public String description = "";
    public String categoryKey = "";
    public String projectKey = "";
    public long timestamp = 0;
    public String owner = "";

    @Override
    public int compareTo(@NonNull Item item) {
        if (timestamp == 0)
            return 0;

        Date a = new Date();
        a.setTime(timestamp);
        Date b = new Date();
        b.setTime(item.timestamp);
        if (a.before(b))
            return -1;
        else if (a.after(b)) // it's equals
            return 1;
        else
            return 0;
    }
}
