package vn.digitalstep.abtask.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by LIO1HC on 11/27/2017.
 */

public class Category implements Comparable<Category>, Serializable {
    public Category() {
    }

    public String key = "";
    public String name = "";
    public String description = "";
    public String projectKey = "";
    public long timestamp = 0;
    public String owner = "";

    @Override
    public int compareTo(@NonNull Category category) {
        if (timestamp == 0)
            return 0;

        Date a = new Date();
        a.setTime(timestamp);
        Date b = new Date();
        b.setTime(category.timestamp);
        if (a.before(b))
            return -1;
        else if (a.after(b)) // it's equals
            return 1;
        else
            return 0;
    }
}
