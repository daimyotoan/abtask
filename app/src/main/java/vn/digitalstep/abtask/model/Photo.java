package vn.digitalstep.abtask.model;

public class Photo {
    public Photo(){}
    public String key = "";
    public String name = "";
    public String url = "";
    public long timestamp = 0;
}