package vn.digitalstep.abtask.model;

/**
 * Created by LIO1HC on 11/26/2017.
 */

public class User {
    public User() {
    }

    public String uid = "";
    public String email = "";
    public String fullName = "";
    public String phone = "";
    public long timestamp = 0;
    public String urlPhoto = "";
    public String departmentKey = "";
    public String projectKey = "";
    public String categoryKey = "";
    public String itemKey = "";
}
