package vn.digitalstep.abtask.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ProgressBar;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.util.Util;

/**
 * Created by Toan on 2/28/2016.
 */
public class DelayAutoCompleteTextView extends android.support.v7.widget.AppCompatAutoCompleteTextView{
    private static final int MESSAGE_TEXT_CHANGED = 100;
    private static final int DEFAULT_AUTOCOMPLETE_DELAY = 500;

    private int mAutoCompleteDelay = DEFAULT_AUTOCOMPLETE_DELAY;
    private ProgressBar mLoadingIndicator;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            DelayAutoCompleteTextView.super.performFiltering((CharSequence) msg.obj, msg.arg1);
        }
    };

    public DelayAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(context, attrs);
    }

    public void setLoadingIndicator(ProgressBar progressBar) {
        mLoadingIndicator = progressBar;
    }

    public void setAutoCompleteDelay(int autoCompleteDelay) {
        mAutoCompleteDelay = autoCompleteDelay;
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        if (mLoadingIndicator != null) {
            mLoadingIndicator.setVisibility(View.VISIBLE);
        }
        mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_CHANGED, text), mAutoCompleteDelay);
    }

    @Override
    public void onFilterComplete(int count) {
        if (mLoadingIndicator != null) {
            mLoadingIndicator.setVisibility(View.GONE);
        }
        super.onFilterComplete(count);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        this.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_small));
        this.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextViewWithTypeFace,
                0, 0);

        try {
            String tf = attributes.getString(R.styleable.TextViewWithTypeFace_typeface);
            if(tf != null && !tf.equals("")) {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + tf);
                Util.setTextViewTypeface(this, typeface);
            }
            else{
//                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + strTypeface);
//                Util.setTextViewTypeface(this, typeface);
            }
        } finally {
            attributes.recycle();
        }
    }
}
