package vn.digitalstep.abtask.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.EditText;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.config.Config;
import vn.digitalstep.abtask.util.Util;


/**
 * Created by Toan on 3/9/2016.
 */
public class AutoDecimalEditText extends android.support.v7.widget.AppCompatEditText {
    String strTypeface = Config.DEFAULT_FONT;
    public MTextWatcher amountWatcher = new MTextWatcher(this);
    private Context _context;

    private void init(){
        if(MDecimalFormat.decimalChar == ',') {
            setKeyListener(DigitsKeyListener.getInstance("0123456789,-"));
        } else {
            setKeyListener(DigitsKeyListener.getInstance("0123456789.-"));
        }
    }

    public interface DecimalTextChangedListener {
        public void OnTextChanged(EditText v);
    }

    DecimalTextChangedListener textChangedListener;
    public void setOnDecimalTextChangedListener(DecimalTextChangedListener listener){
        textChangedListener = listener;
        amountWatcher.setTextChangedListener(new MTextWatcher.TextChangedListener() {
            @Override
            public void OnTextChanged(EditText v) {
                if (textChangedListener != null)
                    textChangedListener.OnTextChanged(AutoDecimalEditText.this);
            }
        });
    }

    public AutoDecimalEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setText("0");
        init();
        initAttributes(context, attrs);
        addTextChangedListener(amountWatcher);
    }

    public AutoDecimalEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setText("0");
        init();
        initAttributes(context, attrs);
        addTextChangedListener(amountWatcher);
    }

    public AutoDecimalEditText(Context context) {
        super(context);
        setText("0");
        init();
        addTextChangedListener(amountWatcher);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        this._context = context;
        this.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_small));
        this.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextViewWithTypeFace,
                0, 0);

        try {
            String tf = attributes.getString(R.styleable.TextViewWithTypeFace_typeface);
            if(tf != null && !tf.equals("")) {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + tf);
                Util.setTextViewTypeface(this, typeface);
            }
            else{
//                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + strTypeface);
//                Util.setTextViewTypeface(this, typeface);
            }
        } finally {
            attributes.recycle();
        }
    }
}
