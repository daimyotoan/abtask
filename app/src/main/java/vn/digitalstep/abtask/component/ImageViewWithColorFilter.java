package vn.digitalstep.abtask.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import vn.digitalstep.abtask.R;


/**
 * Created by lio1hc on 7/28/2016.
 */
public class ImageViewWithColorFilter extends android.support.v7.widget.AppCompatImageView {
    int colorFilter = 0;
    private Context _context;

    public ImageViewWithColorFilter(Context context) {
        super(context);
    }

    public ImageViewWithColorFilter(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(context, attrs);
    }

    public ImageViewWithColorFilter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(context, attrs);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        this._context = context;
        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ImageViewWithColorFilter,
                0, 0);

        try {
            colorFilter = attributes.getColor(R.styleable.ImageViewWithColorFilter_filterColor,
                    getResources().getColor(R.color.colorAccent));
            this.setColorFilter(colorFilter, PorterDuff.Mode.MULTIPLY);
        } finally {
            attributes.recycle();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        try {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_DOWN) {
                Animation ani = AnimationUtils.loadAnimation(this.getContext(), R.anim.scale_animation_button_action_down);
                ani.setFillEnabled(true);
                ani.setFillAfter(true);
                this.startAnimation(ani);
            } else if (action == MotionEvent.ACTION_UP) {
                Animation ani = AnimationUtils.loadAnimation(this.getContext(), R.anim.scale_animation_button_action_up);
                ani.setFillEnabled(true);
                ani.setFillAfter(true);
                this.startAnimation(ani);
            }
        } catch (Exception ex) {
            Log.d("TOUCH_IMAGE", ex.getMessage());
        }
        return super.dispatchTouchEvent(event);
    }
}
