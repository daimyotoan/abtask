package vn.digitalstep.abtask.component;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

public class MDecimalFormat {

    // private static NumberFormat nf_in =
    // NumberFormat.getNumberInstance(Locale.FRANCE);
    public static char groupChar = ',';
    public static char decimalChar = '.';
    private static DecimalFormatSymbols otherSymbols = null;

    public static void setDecimalSeparator(char c) {
        if (c == '.') {
            decimalChar = '.';
            groupChar = ',';

            if (otherSymbols != null) {
                otherSymbols.setDecimalSeparator('.');
                otherSymbols.setGroupingSeparator(',');
            }
            df = new DecimalFormat("###,###,###,###.##", otherSymbols);
        } else {
            decimalChar = ',';
            groupChar = '.';

            if (otherSymbols != null) {
                otherSymbols.setDecimalSeparator(',');
                otherSymbols.setGroupingSeparator('.');
            }

            df = new DecimalFormat("###.###.###.###,##", otherSymbols);
        }
    }

    public static void SetCurrentLocale(Locale lc) {
        otherSymbols = new DecimalFormatSymbols(lc);
        // nf_in = NumberFormat.getNumberInstance(lc);
        // df = (DecimalFormat) DecimalFormat.getInstance();
    }

    private static DecimalFormat df = new DecimalFormat("###,###,###,###.##");

    public static String format(Double value) {
        if (value == null)
            return "0";
        return df.format(value);
    }

    public static String format(double value) {
        // return nf_in.format(value);
        return df.format(value);
    }

    public static String format(long value) {
        // return nf_in.format(value);
        return df.format(value);
    }

    public static double parse(CharSequence c_number) throws ParseException {
        String str_number = c_number.toString();
        if ("".equals(str_number.trim())) {
            return 0;
        }
        String[] strs = str_number.split(Character.toString(decimalChar));
        StringBuffer sb = new StringBuffer();
        if (strs.length == 1) {
            sb.append(strs[0].replaceAll("\\" + groupChar, ""));
        } else if (strs.length == 0) {
            String[] ks = str_number.split(Character.toString(groupChar));
            for (String s : ks) {
                sb.append(s);
            }
        } else if (strs.length == 2) {
            sb.append(strs[0].replaceAll("\\" + groupChar, ""));
            sb.append(Character.toString(decimalChar));
            sb.append(strs[1]);
        } else {
            throw new ParseException("quanlytro", 21);
        }
        return df.parse(sb.toString()).doubleValue();
    }

	/*
     * public static String formatPercent(double value) { // return
	 * nf_in.format(value); return df1.format(value); }
	 */
}
