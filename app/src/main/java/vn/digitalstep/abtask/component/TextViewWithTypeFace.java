package vn.digitalstep.abtask.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.config.Config;
import vn.digitalstep.abtask.util.Util;

/**
 * Created by lio1hc on 7/28/2016.
 */
public class TextViewWithTypeFace extends android.support.v7.widget.AppCompatTextView{
    String strTypeface = Config.DEFAULT_FONT;
    private Context _context;

    public TextViewWithTypeFace(Context context) {
        super(context);
    }

    public TextViewWithTypeFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(context, attrs);
    }

    public TextViewWithTypeFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(context, attrs);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        this._context = context;
        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextViewWithTypeFace,
                0, 0);

        try {
            String tf = attributes.getString(R.styleable.TextViewWithTypeFace_typeface);
            if(tf != null && !tf.equals("")) {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + tf);
                Util.setTextViewTypeface(this, typeface);
            }
            else{
//                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + strTypeface);
//                Util.setTextViewTypeface(this, typeface);
            }
        } finally {
            attributes.recycle();
        }
    }
}
