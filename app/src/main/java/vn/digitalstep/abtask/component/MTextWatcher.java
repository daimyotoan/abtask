package vn.digitalstep.abtask.component;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.ParseException;

/**
 * Created by LIO1HC on 10/13/2017.
 */

public class MTextWatcher implements TextWatcher {

    public interface TextChangedListener {
        public void OnTextChanged(EditText v);
    }

    EditText et;
    TextChangedListener _listener = null;
    ;

    public MTextWatcher(EditText editText) {
        et = editText;
    }

    public void setTextChangedListener(TextChangedListener listener) {
        _listener = listener;
    }

    @Override
    public void afterTextChanged(Editable s) {
        et.removeTextChangedListener(this);
        try {
            int inilen, endlen;
            inilen = et.getText().length();

            String v = s.toString();

            String[] splittedValues = null;
            if (MDecimalFormat.decimalChar == '.') {
                splittedValues = v.split("\\.");
            } else {
                splittedValues = v.split(",");
            }

            String newText = "0";

            int cp = et.getSelectionStart();

            if (splittedValues.length == 1) {
                double n = MDecimalFormat.parse(v);
                if (v.contains(MDecimalFormat.decimalChar + "")) {
                    newText = MDecimalFormat.format(n) + MDecimalFormat.decimalChar;
                    et.setText(newText);
                } else {
                    newText = MDecimalFormat.format(n);
                    et.setText(newText);
                }

            } else if (splittedValues.length >= 2) {
                double number = MDecimalFormat.parse(splittedValues[0]);
                double decimal = MDecimalFormat.parse("0" + MDecimalFormat.decimalChar + splittedValues[1]);
                newText = MDecimalFormat.format(number) + MDecimalFormat.decimalChar + splittedValues[1];
                et.setText(newText);
            }


//            if (MDecimalFormat.decimalChar == '.' && !v.contains("."))
//                et.setText(MDecimalFormat.format(n));
//
//            if (MDecimalFormat.decimalChar == ',' && !v.contains(","))
//                et.setText(MDecimalFormat.format(n));

            endlen = newText.length();
            int sel = (cp + (endlen - inilen));
            if (sel > 0 && sel <= newText.length()) {
                et.setSelection(sel);
            } else {
                // place cursor at the end?
                et.setSelection(newText.length());
            }

            //et.setSelection(et.getText().length());
            if (_listener != null) {
                _listener.OnTextChanged(et);
            }
        } catch (NumberFormatException nfe) {
            // do nothing?
        } catch (ParseException e) {
            // do nothing?
        }

        et.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

}