package vn.digitalstep.abtask.fragment;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.adapter.ProjectItemAdapter;
import vn.digitalstep.abtask.config.Config;
import vn.digitalstep.abtask.dialog.DlgEditProject;
import vn.digitalstep.abtask.dialog.DlgNewProject;
import vn.digitalstep.abtask.dialog.WSConfig;
import vn.digitalstep.abtask.model.Project;
import vn.digitalstep.abtask.model.User;
import vn.digitalstep.abtask.util.BitmapUtils;
import vn.digitalstep.abtask.util.PermissionUtil;
import vn.digitalstep.abtask.util.Util;

/**
 * Created by LIO1HC on 11/27/2017.
 */

public class FragmentProject extends Fragment {
    private View rootView;
    private FirebaseUser fUser;
    private DatabaseReference dataRef;
    private User mUser;
    private ProjectItemAdapter projectItemAdapter;
    private ArrayList<Project> projects = new ArrayList<>();
    private RecyclerView rvProjects;
    private DlgNewProject dlgNewProject;
    private DlgEditProject dlgEditProject;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_project, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFirebase();
        initView();
    }

    private void initView() {
        Util.showLoading(getActivity());
        dataRef.child(WSConfig.PATH_USER).child(fUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Util.hideLoading();
                if (dataSnapshot.getValue() != null) {
                    mUser = dataSnapshot.getValue(User.class);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Util.hideLoading();
            }
        });

        rootView.findViewById(R.id.fab_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgNewProject = new DlgNewProject(getActivity());
                dlgNewProject.show();
                dlgNewProject.setListener(new DlgNewProject.OnDlgNewProject() {
                    @Override
                    public void onProjectCreated() {
                        loadProjects();
                    }
                });
            }
        });

        rvProjects = (RecyclerView) rootView.findViewById(R.id.rv_projects);
        projectItemAdapter = new ProjectItemAdapter(getActivity(), projects);
        projectItemAdapter.setListener(listener);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvProjects.setLayoutManager(mLayoutManager);
        rvProjects.setItemAnimator(new DefaultItemAnimator());
        rvProjects.setAdapter(projectItemAdapter);

        loadProjects();
    }

    private void loadProjects() {
        projects.clear();
        Util.showLoading(getActivity());
        dataRef.child(WSConfig.PATH_PROJECTS).child(fUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.getValue() != null) {
                            Project project = snapshot.getValue(Project.class);
                            if (project.status == 0) {
                                projects.add(project);
                            }
                        }
                    }
                    Util.sortProjectDesc(projects);
                    projectItemAdapter.notifyDataSetChanged();
                }
                Util.hideLoading();

                if (projects.size() == 0) {
                    rootView.findViewById(R.id.tv_no_project).setVisibility(View.VISIBLE);
                } else {

                    rootView.findViewById(R.id.tv_no_project).setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Util.hideLoading();
            }
        });
    }

    private ProjectItemAdapter.ProjectItemAdapterListener listener = new ProjectItemAdapter.ProjectItemAdapterListener() {
        @Override
        public void onSelect(Project p) {
            dlgEditProject = new DlgEditProject(getActivity(), p);
            dlgEditProject.show();
            dlgEditProject.setListener(new DlgEditProject.OnDlgEditProject() {
                @Override
                public void onUpdated() {
                    loadProjects();
                }
            });
        }
    };

    private void initFirebase() {
        dataRef = FirebaseDatabase.getInstance().getReference();
        fUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                default:
                    break;
            }
        }
    };

    private PopupMenu.OnMenuItemClickListener popupItemClickListener = new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                default:
                    break;
            }
            return false;
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.DLG_NEW_PRJ_PICKFILE_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                int photo_size = this.getResources().getDimensionPixelSize(R.dimen._64sdp);
                Bitmap scaledBitmap = BitmapUtils.getScaledBitmapRatio(bitmap, photo_size);
                boolean res = PermissionUtil.getPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE,
                        PermissionUtil.PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL);
                if (dlgNewProject == null)
                    return;
                switch (dlgNewProject.imgClicked.getId()) {
                    case R.id.img_photo1:
                        dlgNewProject.bitmaps[0] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo2:
                        dlgNewProject.bitmaps[1] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo3:
                        dlgNewProject.bitmaps[2] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo4:
                        dlgNewProject.bitmaps[3] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    default:
                        break;
                }
            } catch (IOException e) {
                Log.e("GALLERY_PICKED", e.getMessage());
            }
        }

        if (requestCode == Config.DLG_NEW_PRJ_CAMERA_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            try {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                int photo_size = this.getResources().getDimensionPixelSize(R.dimen._200sdp);
                Bitmap scaledBitmap = BitmapUtils.getScaledBitmapRatio(bitmap, photo_size);
                boolean res = PermissionUtil.getPermission(getActivity(), Manifest.permission.CAMERA,
                        PermissionUtil.PERMISSIONS_REQUEST_CAMERA);
                if (dlgNewProject == null)
                    return;
                switch (dlgNewProject.imgClicked.getId()) {
                    case R.id.img_photo1:
                        dlgNewProject.bitmaps[0] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo2:
                        dlgNewProject.bitmaps[1] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo3:
                        dlgNewProject.bitmaps[2] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo4:
                        dlgNewProject.bitmaps[3] = scaledBitmap;
                        dlgNewProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                Log.e("CAMERA TAKEN", e.getMessage());
            }
        }

        if (requestCode == Config.DLG_EDT_PRJ_PICKFILE_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                int photo_size = this.getResources().getDimensionPixelSize(R.dimen._64sdp);
                Bitmap scaledBitmap = BitmapUtils.getScaledBitmapRatio(bitmap, photo_size);
                boolean res = PermissionUtil.getPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE,
                        PermissionUtil.PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL);
                if (dlgEditProject == null)
                    return;
                switch (dlgEditProject.imgClicked.getId()) {
                    case R.id.img_photo1:
                        dlgEditProject.bitmaps[0] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo2:
                        dlgEditProject.bitmaps[1] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo3:
                        dlgEditProject.bitmaps[2] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo4:
                        dlgEditProject.bitmaps[3] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    default:
                        break;
                }
            } catch (IOException e) {
                Log.e("GALLERY_PICKED", e.getMessage());
            }
        }

        if (requestCode == Config.DLG_EDT_PRJ_CAMERA_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            try {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                int photo_size = this.getResources().getDimensionPixelSize(R.dimen._200sdp);
                Bitmap scaledBitmap = BitmapUtils.getScaledBitmapRatio(bitmap, photo_size);
                boolean res = PermissionUtil.getPermission(getActivity(), Manifest.permission.CAMERA,
                        PermissionUtil.PERMISSIONS_REQUEST_CAMERA);
                if (dlgEditProject == null)
                    return;
                switch (dlgEditProject.imgClicked.getId()) {
                    case R.id.img_photo1:
                        dlgEditProject.bitmaps[0] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo2:
                        dlgEditProject.bitmaps[1] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo3:
                        dlgEditProject.bitmaps[2] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    case R.id.img_photo4:
                        dlgEditProject.bitmaps[3] = scaledBitmap;
                        dlgEditProject.imgClicked.setImageBitmap(scaledBitmap);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                Log.e("CAMERA TAKEN", e.getMessage());
            }
        }
    }

    private void uploadProfileImage(Bitmap bitmap) {
        if (!Util.isOnline(getActivity()))
            return;

        Util.showLoading(getActivity());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();

        StorageReference fileRef = FirebaseStorage.getInstance().getReference()
                .child(WSConfig.STORAGE_PROJECT_PHOTO + "/" + fUser.getUid());
        UploadTask uploadTask = fileRef.putBytes(data);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                //add node to DB
                StorageMetadata metadata = taskSnapshot.getMetadata();
                String url = metadata.getDownloadUrl().toString();
                dataRef.child(WSConfig.PATH_USER).child(fUser.getUid()).child("urlPhoto").setValue(url);
                mUser.urlPhoto = url;
                Util.hideLoading();
            }
        });
    }

}
