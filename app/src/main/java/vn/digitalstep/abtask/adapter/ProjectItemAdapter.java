package vn.digitalstep.abtask.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.model.Project;

/**
 * Created by Toan on 3/7/2016.
 */
public class ProjectItemAdapter extends RecyclerView.Adapter<ProjectItemAdapter.MyViewHolder> {
    Activity mActivity;
    private ArrayList<Project> projects;
    private FirebaseUser fUser;
    private DatabaseReference dataRef;
    private ProjectItemAdapterListener listener;

    public ProjectItemAdapter(Activity mActivity, ArrayList<Project> projects) {
        this.mActivity = mActivity;
        this.projects = projects;
        fUser = FirebaseAuth.getInstance().getCurrentUser();
        dataRef = FirebaseDatabase.getInstance().getReference();
    }

    public interface ProjectItemAdapterListener {
        public void onSelect(Project p);
    }

    public void setListener(ProjectItemAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_item_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Project project = projects.get(position);
        holder.name.setText(project.name);
        holder.desc.setText(project.description);
        if (!project.urlPhoto.equals("")) {
            Glide.with(mActivity).load(project.urlPhoto).into(holder.imgPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, desc, timestamp;
        public ImageView imgPhoto;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_project_name);
            desc = (TextView) view.findViewById(R.id.tv_project_desc);
            timestamp = (TextView) view.findViewById(R.id.tv_timestamp);
            imgPhoto = (ImageView) view.findViewById(R.id.img_photo);
        }

        @Override
        public void onClick(View view) {
            Project project = projects.get(getLayoutPosition());
            listener.onSelect(project);
        }
    }

}
