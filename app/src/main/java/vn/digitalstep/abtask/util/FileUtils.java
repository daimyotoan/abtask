package vn.digitalstep.abtask.util;

import java.io.File;

/**
 * Created by lio1hc on 6/23/2016.
 */
public class FileUtils {
    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public static boolean isAlreadyDownloaded(String title, String path) {
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].getName().equals(title))
                    return true;
            }
        }
        return false;
    }

    public static String getFileExtension(String filename) {
        try {
            return filename.substring(filename.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    public static void createFolder(String path) {
        File f = new File(path);
        if (!f.exists() || !f.isDirectory())
            f.mkdir();
    }
}
