package vn.digitalstep.abtask.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.config.Config;
import vn.digitalstep.abtask.model.Project;

/**
 * Created by LIO1HC on 10/8/2016.
 */
public class Util {
    private static Util instance = null;
    private static ProgressDialog progress;

    public static Util getInstance() {
        if (instance == null)
            instance = new Util();
        return instance;
    }

    public static void setViewGroupTypeface(ViewGroup container, Typeface typeface) {
        final int children = container.getChildCount();

        for (int i = 0; i < children; i++) {
            View child = container.getChildAt(i);

            if (child instanceof TextView) {
                setTextViewTypeface((TextView) child, typeface);
            } else if (child instanceof ViewGroup) {
                setViewGroupTypeface((ViewGroup) child, typeface);
            }
        }
    }

    public static void setTextViewTypeface(TextView textView, Typeface typeface) {
        textView.setTypeface(typeface);
    }

    public static void showLoading(Context context) {
        if (!Util.isOnline(context)) {
            hideLoading();
            return;
        }
        if (progress == null || !progress.isShowing()) {
            String message = context.getResources().getString(R.string.please_wait_loading);
            progress = new ProgressDialog(context);
            progress.setMessage(message);
            progress.show();
        }
    }

    public static void showLoadingWithText(Context context, int resString) {
        if (!Util.isOnline(context)) {
            hideLoading();
            return;
        }
        if (progress == null || !progress.isShowing()) {
            String message = context.getResources().getString(resString);
            progress = new ProgressDialog(context);
            progress.setMessage(message);
            progress.show();
        }
    }

    public static void hideLoading() {
        if (progress.isShowing()) {
            progress.dismiss();
        }
    }

    public static String formatCurrency(long value) {
        return String.format("%,d", value);
    }

    public static long getNumberFromCurrency(String currency) {
        try {
            String s = currency.replace("đ", "").trim().replace(",", "");
            String[] strings = s.split(" ");
            return Long.parseLong(strings[0]);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static String getDateInString(long time) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String date = formatter.format(new Date(time));
        return date;
    }

    public static String getDateInMonthPeriod(long time) {
        DateFormat formatter = new SimpleDateFormat("MM/yyyy");
        String date = formatter.format(new Date(time));
        return date;
    }

    public static String getMonthDBPath(long time) {
        DateFormat formatter = new SimpleDateFormat("MM_yyyy");
        String date = formatter.format(new Date(time));
        return date;
    }

    public static int getDayofDate(long time) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static String getDateTimeInString(long time) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String date = formatter.format(new Date(time));
        return date;
    }

    public static void setDrawableFilterColor(Activity activity, Drawable d, int intColor) {
        int color = ContextCompat.getColor(activity, intColor);
        d.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
    }

    public static Bitmap getScaledBitmapRatio(Bitmap bitmap, int scaleSize) {
        Bitmap resizedBitmap = null;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        float multFactor = -1.0F;
        if (originalHeight > originalWidth) {
            newHeight = scaleSize;
            multFactor = (float) originalWidth / (float) originalHeight;
            newWidth = (int) (newHeight * multFactor);
        } else if (originalWidth > originalHeight) {
            newWidth = scaleSize;
            multFactor = (float) originalHeight / (float) originalWidth;
            newHeight = (int) (newWidth * multFactor);
        } else if (originalHeight == originalWidth) {
            newHeight = scaleSize;
            newWidth = scaleSize;
        }
        resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
        return resizedBitmap;
    }

    public static Bitmap storeImage(Bitmap bitmap, File filename) {
        //permission already granted

        if (filename.exists())
            filename.delete();
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    public static boolean isOnline(Context ctx) {
        if (ctx == null)
            return false;

        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            Toast.makeText(ctx, R.string.no_internet, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception exception) {
            Log.d("hide_keyboard", exception.getMessage());
        }
    }

    public static void callPhoneNumber(Context context, String phone) {
        if (!phone.equals("") && !phone.equals(" ") && !phone.equals("0")) {
            String strFirstPhone = phone;
            if (strFirstPhone.contains("-")) {
                strFirstPhone = strFirstPhone.substring(0, strFirstPhone.indexOf("-"));
            }
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", strFirstPhone, null));
            context.startActivity(intent);
        }
    }

    public static void sendMessage(Context context, String phone) {
        if (!phone.equals("") && !phone.equals(" ") && !phone.equals("0")) {
            String strFirstPhone = phone;
            if (strFirstPhone.contains("-")) {
                strFirstPhone = strFirstPhone.substring(0, strFirstPhone.indexOf("-"));
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", strFirstPhone, null));
            context.startActivity(intent);
        }
    }

    public static void sendMessageWithContent(Context context, String phone, String content) {
        if (!phone.equals("") && !phone.equals(" ") && !phone.equals("0")) {
            String strFirstPhone = phone;
            if (strFirstPhone.contains("-")) {
                strFirstPhone = strFirstPhone.substring(0, strFirstPhone.indexOf("-"));
            }
            Uri uri = Uri.parse("smsto:" + strFirstPhone);
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            it.putExtra("sms_body", content);
            context.startActivity(it);
        }
    }

    public static boolean isSameMonthPeriod(long t1, long t2) {
        String s1 = getDateInMonthPeriod(t1);
        String s2 = getDateInMonthPeriod(t2);
        if (s1.equals(s2))
            return true;
        else
            return false;
    }

    public static int numRentingDayWithoutYear(long enterday, long present) {
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(enterday);
        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(present);

        int date_of_enterday = c1.get(Calendar.DAY_OF_MONTH);
        int date_of_present = c2.get(Calendar.DAY_OF_MONTH);

        if (date_of_enterday <= date_of_present) {
            return date_of_present - date_of_enterday;
        } else {
            int num_day_of_last_month = numDayOfLastMonth();
            return (date_of_present + num_day_of_last_month) - date_of_enterday;
        }
    }

    public static int numDayOfLastMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        return c.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static String removeAccent(String s) {
        return Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    public static BitSet convertBitmap(Bitmap inputBitmap) {

        int mWidth = inputBitmap.getWidth();
        int mHeight = inputBitmap.getHeight();

        return convertArgbToGrayscale(inputBitmap, mWidth, mHeight);
    }

    private static BitSet convertArgbToGrayscale(Bitmap bmpOriginal, int width, int height) {
        int pixel;
        int k = 0;
        int B = 0, G = 0, R = 0;
        BitSet dots = new BitSet();
        try {

            for (int x = 0; x < height; x++) {
                for (int y = 0; y < width; y++) {
                    // get one pixel color
                    pixel = bmpOriginal.getPixel(y, x);

                    // retrieve color of all channels
                    R = Color.red(pixel);
                    G = Color.green(pixel);
                    B = Color.blue(pixel);
                    // take conversion up to one single value by calculating
                    // pixel intensity.
                    R = G = B = (int) (0.299 * R + 0.587 * G + 0.114 * B);
                    // set bit into bitset, by calculating the pixel's luma
                    if (R < 100) {
                        dots.set(k);// this is the bitset that i'm printing
                    }
                    k++;

                }

            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.v("minishop", e.toString(), e);
        }
        return dots;
    }

    public static long getBeginningTime(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }

    public static long getEndingTime(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 99);
        return c.getTimeInMillis();
    }

    public static Fragment getTopFragment(FragmentActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager != null && fragmentManager.getBackStackEntryCount() == 0) {
            return null;
        }
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        return fragmentManager.findFragmentByTag(fragmentTag);
    }

    public static void sendFeedback(Activity mActivity) {
        String[] addresses = new String[10];
        addresses[0] = Config.EMAIL_SUPPORT;
        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, Config.EMAIL_SUBJECT);
        intent.putExtra(Intent.EXTRA_TEXT, "");
        mActivity.startActivity(intent);
    }

    public static void sortProjectDesc(ArrayList<Project> map) {
        if (map == null)
            return;
        try {
            Collections.sort(map);
            Collections.reverse(map);
        } catch (Exception ex) {
            Log.e("sortProjectDesc", ex.getMessage());
        }
    }
}
