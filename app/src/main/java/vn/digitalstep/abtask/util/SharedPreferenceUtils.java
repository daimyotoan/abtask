package vn.digitalstep.abtask.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtils {

    public static final String APP_KEY = "vn.digitalstep.abtask";
    public static final String REGISTER_KEY = "vn.digitalstep.abtask.REGISTER";

    public static void setLogin(Context context, boolean login) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(REGISTER_KEY, login);
        editor.commit();
    }

    public static boolean isLoggedIn(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(REGISTER_KEY, false);
    }

}
