package vn.digitalstep.abtask.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.dialog.DlgSimpleMessage;


/**
 * Created by toan on 5/9/2016.
 */
public class PermissionUtil {

    public static final int PERMISSIONS_REQUEST_ALL = 0;
    public static final int PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL = 100;
    public static final int PERMISSIONS_REQUEST_CAMERA = 101;
    public static final int PERMISSIONS_REQUEST_PHONE_STATE = 102;
    public static final int PERMISSIONS_REQUEST_CONTACT = 103;
    public static final int PERMISSIONS_LOCATION = 104;

    public static boolean getPermission(Activity activity, String permission, int code){
        boolean res = false;
        if (ContextCompat.checkSelfPermission(activity, permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    permission)) {

                DlgSimpleMessage dlg;
                switch (permission){
                    case Manifest.permission.CAMERA:
                        dlg = new DlgSimpleMessage(activity, R.string.explanation_title,
                                R.string.explanation_camera_permission);
                        dlg.show();
                        break;
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
                        dlg = new DlgSimpleMessage(activity, R.string.explanation_title,
                                R.string.explanation_readwrite_external_permission);
                        dlg.show();
                        break;
                    case Manifest.permission.READ_PHONE_STATE:
                        dlg = new DlgSimpleMessage(activity, R.string.explanation_title,
                                R.string.explanation_phone_state_permission);
                        dlg.show();
                        break;
                    case Manifest.permission.ACCESS_FINE_LOCATION:
                    case Manifest.permission.ACCESS_COARSE_LOCATION:
                        dlg = new DlgSimpleMessage(activity, R.string.explanation_title,
                                R.string.explanation_location_permission);
                        dlg.show();
                        break;
                }
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(activity,
                        new String[]{permission}, code);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity,
                        new String[]{permission}, code);
            }
        }
        else{
            res = true;
        }
        return res;
    }

}
