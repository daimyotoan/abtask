package vn.digitalstep.abtask.config;

/**
 * Created by LIO1HC on 11/26/2017.
 */

public class Config {
    private static Config instance = null;
    public static int PICKFILE_REQUEST_CODE = 98;
    public static int CAMERA_REQUEST_CODE = 99;
    public static int DLG_NEW_PRJ_PICKFILE_REQUEST_CODE = 100;
    public static int DLG_NEW_PRJ_CAMERA_REQUEST_CODE = 101;
    public static int DLG_EDT_PRJ_PICKFILE_REQUEST_CODE = 102;
    public static int DLG_EDT_PRJ_CAMERA_REQUEST_CODE = 103;
    public static final String DEFAULT_FONT = "arial.ttf";
    public final static String EMAIL_SUPPORT = "lieu.toan.vn@gmail.com";
    public static String EMAIL_SUBJECT = "Góp ý #";
    public static Config getInstance(){
        if(instance == null)
            instance = new Config();
        return instance;
    }
}
