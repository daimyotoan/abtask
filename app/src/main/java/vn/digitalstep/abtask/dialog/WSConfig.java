package vn.digitalstep.abtask.dialog;

/**
 * Created by LIO1HC on 11/26/2017.
 */

public class WSConfig {
    private static WSConfig instance = null;
    public static final String PATH_USER = "users";
    public static final String PATH_PROJECTS = "projects";
    public static final String PATH_CATEGORY = "categories";
    public static final String PATH_ITEM = "items";
    public static final String PATH_DEPARTMENT = "departments";
    public static final String PATH_PROJECT_PHOTO = "project_photos";
    public static final String STORAGE_PROJECT_PHOTO = "project_photos";

    public static WSConfig getInstance() {
        if (instance == null) {
            instance = new WSConfig();
        }
        return instance;
    }
}
