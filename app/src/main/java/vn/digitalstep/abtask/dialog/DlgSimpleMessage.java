package vn.digitalstep.abtask.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import vn.digitalstep.abtask.R;

public class DlgSimpleMessage {

    Context mContext;
    int mTitleId;
    int mMessageId;
    int mIconId;
    String mStrMessage;
    DialogSimpleMessageListener mListener;
    AlertDialog.Builder builder;
    private AlertDialog mDialog;

    public interface DialogSimpleMessageListener {
        public void onClose();

        public void onConfirm();
    }

    public DlgSimpleMessage(Context context, int titleId, int messageId) {
        mContext = context;
        mTitleId = titleId;
        mMessageId = messageId;
    }

    public DlgSimpleMessage(Context context, int titleId, String message) {
        mContext = context;
        mTitleId = titleId;
        mStrMessage = message;
    }

    public DlgSimpleMessage(Context context, int titleId, int mMessageId, int mIconId) {
        mContext = context;
        mTitleId = titleId;
        this.mMessageId = mMessageId;
        this.mIconId = mIconId;
    }

    public void setListener(DialogSimpleMessageListener l) {
        mListener = l;
    }

    public void show() {
        builder = new AlertDialog.Builder(mContext);
        if (mTitleId != 0)
            builder.setTitle(mTitleId);
        if (mIconId != 0)
            builder.setIcon(mIconId);
        if (mStrMessage != null) {
            builder.setMessage(mStrMessage);
        }
        if (mMessageId != 0) {
            builder.setMessage(mMessageId);
        }
        builder.setNegativeButton(R.string.btn_cancel, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListener != null) {
                    mDialog.dismiss();
                    mListener.onClose();
                }
            }
        });
        builder.setPositiveButton(R.string.btn_ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListener != null) {
                    mDialog.dismiss();
                    mListener.onConfirm();
                }
            }
        });

        mDialog = builder.create();
        mDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        mDialog.show();
    }

    public void setCancelable(boolean b) {
        mDialog.setCancelable(b);
    }

}
