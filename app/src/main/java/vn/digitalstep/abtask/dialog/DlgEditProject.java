package vn.digitalstep.abtask.dialog;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.activity.MainActivity;
import vn.digitalstep.abtask.config.Config;
import vn.digitalstep.abtask.model.Photo;
import vn.digitalstep.abtask.model.Project;
import vn.digitalstep.abtask.util.PermissionUtil;
import vn.digitalstep.abtask.util.Util;

/**
 * Created by LIO1HC on 10/9/2016.
 */
public class DlgEditProject {
    private Activity mActivity;
    private View mRootView;
    private AlertDialog mDialog;
    private FirebaseUser fUser;
    private DatabaseReference dataRef;
    private StorageReference storageRef;
    private OnDlgEditProject listener;
    private ImageView img1, img2, img3, img4;
    public ImageView imgClicked;
    public Bitmap[] bitmaps = new Bitmap[4];
    private Project project;

    public DlgEditProject(Activity mActivity, Project project) {
        this.mActivity = mActivity;
        this.project = project;
        fUser = FirebaseAuth.getInstance().getCurrentUser();
        dataRef = FirebaseDatabase.getInstance().getReference();
        storageRef = FirebaseStorage.getInstance().getReference();
    }

    public interface OnDlgEditProject {
        void onUpdated();
    }

    public void setListener(OnDlgEditProject listener) {
        this.listener = listener;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.CustomAlertDialog);
        mRootView = mActivity.getLayoutInflater().inflate(R.layout.dialog_project_info, null);
        builder.setView(mRootView);
        builder.setTitle(R.string.project_info);
        builder.setPositiveButton(R.string.btn_ok, null);
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mDialog.dismiss();
            }
        });

        mDialog = builder.create();
        mDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        Rect displayRectangle = new Rect();
        Window window = mActivity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        WindowManager.LayoutParams wlp = mDialog.getWindow().getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        mDialog.show();
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        mDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

        initView();
    }

    private void submit() {
        String name = ((EditText) mRootView.findViewById(R.id.edt_project_name)).getText().toString();
        String desc = ((EditText) mRootView.findViewById(R.id.edt_project_desc)).getText().toString();

        if (name.equals("")) {
            ((EditText) mRootView.findViewById(R.id.edt_project_name))
                    .setError(mActivity.getString(R.string.empty_info));
            return;
        }
        if (desc.equals("")) {
            ((EditText) mRootView.findViewById(R.id.edt_project_desc))
                    .setError(mActivity.getString(R.string.empty_info));
            return;
        }

        Project project = this.project;
        project.name = name;
        project.description = desc;
        project.timestamp = new Date().getTime();
        dataRef.child(WSConfig.PATH_PROJECTS).child(fUser.getUid()).child(project.key).setValue(project, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                listener.onUpdated();
            }
        });

        //upload vehicle photos
        if (bitmaps[0] != null) {
            uploadPhoto(bitmaps[0], project.key);
        }
        if (bitmaps[1] != null) {
            uploadPhoto(bitmaps[1], project.key);
        }
        if (bitmaps[2] != null) {
            uploadPhoto(bitmaps[2], project.key);
        }
        if (bitmaps[3] != null) {
            uploadPhoto(bitmaps[3], project.key);
        }
    }

    private void uploadPhoto(final Bitmap bitmap, final String key) {
        if (!Util.isOnline(mActivity))
            return;

        Util.showLoading(mActivity);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        final byte[] data = baos.toByteArray();

        final String photoKey = dataRef.child(WSConfig.PATH_PROJECT_PHOTO).child(key).push().getKey();
        StorageReference fileRef = FirebaseStorage.getInstance().getReference().child(WSConfig.STORAGE_PROJECT_PHOTO + "/" + photoKey);
        UploadTask uploadTask = fileRef.putBytes(data);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                //add node to DB
                StorageMetadata metadata = taskSnapshot.getMetadata();
                String url = metadata.getDownloadUrl().toString();
                Photo photo = new Photo();
                photo.url = url;
                photo.key = photoKey;
                photo.timestamp = new Date().getTime();
                dataRef.child(WSConfig.PATH_PROJECT_PHOTO).child(key).child(photoKey).setValue(photo);
                if (bitmap == bitmaps[0]) {
                    dataRef.child(WSConfig.PATH_PROJECTS).child(fUser.getUid()).child(key)
                            .child("urlPhoto").setValue(photo.url);
                }
                Util.hideLoading();
            }
        });
    }

    private void initView() {
        if (project == null)
            return;
        ((EditText) mRootView.findViewById(R.id.edt_project_name)).setText(project.name);
        ((EditText) mRootView.findViewById(R.id.edt_project_desc)).setText(project.description);

        img1 = (ImageView) mRootView.findViewById(R.id.img_photo1);
        img2 = (ImageView) mRootView.findViewById(R.id.img_photo2);
        img3 = (ImageView) mRootView.findViewById(R.id.img_photo3);
        img4 = (ImageView) mRootView.findViewById(R.id.img_photo4);

        img1.setOnClickListener(onClickListener);
        img2.setOnClickListener(onClickListener);
        img3.setOnClickListener(onClickListener);
        img4.setOnClickListener(onClickListener);

        Util.showLoading(mActivity);
        dataRef.child(WSConfig.PATH_PROJECT_PHOTO).child(project.key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    int count = 0;
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Photo photo = snapshot.getValue(Photo.class);
                        switch (count) {
                            case 0:
                                Glide.with(mActivity).load(photo.url).into(img1);
                                img1.setTag(photo.key);
                                break;
                            case 1:
                                Glide.with(mActivity).load(photo.url).into(img2);
                                img2.setTag(photo.key);
                                break;
                            case 2:
                                Glide.with(mActivity).load(photo.url).into(img3);
                                img3.setTag(photo.key);
                                break;
                            case 3:
                                Glide.with(mActivity).load(photo.url).into(img4);
                                img4.setTag(photo.key);
                                break;
                        }
                        count++;
                    }
                }
                Util.hideLoading();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Util.hideLoading();
            }
        });
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                imgClicked = (ImageView) view;
                if (imgClicked.getTag() != null) {
                    DlgSimpleMessage dlgSimpleMessage = new DlgSimpleMessage(mActivity, 0, R.string.confirm_remove_photo);
                    dlgSimpleMessage.show();
                    dlgSimpleMessage.setListener(new DlgSimpleMessage.DialogSimpleMessageListener() {
                        @Override
                        public void onClose() {

                        }

                        @Override
                        public void onConfirm() {
                            String key = (String) imgClicked.getTag();
                            removePhoto(key);
                        }
                    });

                } else {
                    switch (view.getId()) {
                        case R.id.img_photo1:
                        case R.id.img_photo2:
                        case R.id.img_photo3:
                        case R.id.img_photo4:
                            imgClicked = (ImageView) view;
                            PopupMenu popupMenu = new PopupMenu(mActivity, view);
                            popupMenu.getMenuInflater().inflate(R.menu.popup_menu_image, popupMenu.getMenu());
                            popupMenu.setOnMenuItemClickListener(popupItemClickListener);
                            popupMenu.show();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    };

    private void removePhoto(String key) {
        Util.showLoading(mActivity);
        dataRef.child(WSConfig.PATH_PROJECT_PHOTO).child(project.key).child(key).setValue(null);

        storageRef.child(WSConfig.STORAGE_PROJECT_PHOTO).child(key).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                imgClicked.setImageResource(R.drawable.ic_camera_32);
                imgClicked.setTag(null);
                Util.hideLoading();
            }
        });
    }

    private PopupMenu.OnMenuItemClickListener popupItemClickListener = new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.camera:
                    boolean res = PermissionUtil.getPermission(mActivity, Manifest.permission.CAMERA,
                            PermissionUtil.PERMISSIONS_REQUEST_CAMERA);
                    if (res) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ((MainActivity) mActivity).mCurrentFragment
                                .startActivityForResult(cameraIntent, Config.DLG_EDT_PRJ_CAMERA_REQUEST_CODE);
                    } else {
                        Toast.makeText(mActivity,
                                R.string.denied_permission_dialog_message, Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.gallery:
                    res = true;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        res = PermissionUtil.getPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE,
                                PermissionUtil.PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL);
                    }
                    if (res) {
                        Intent galleryIntent = new Intent();
                        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                        galleryIntent.setType("image/*");
                        ((MainActivity) mActivity).mCurrentFragment
                                .startActivityForResult(galleryIntent, Config.DLG_EDT_PRJ_PICKFILE_REQUEST_CODE);
                    } else {
                        Toast.makeText(mActivity,
                                R.string.denied_permission_dialog_message, Toast.LENGTH_LONG).show();
                    }
                    break;
                default:
                    break;
            }
            return false;
        }
    };
}
