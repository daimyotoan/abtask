package vn.digitalstep.abtask.dialog;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import vn.digitalstep.abtask.R;
import vn.digitalstep.abtask.activity.MainActivity;
import vn.digitalstep.abtask.config.Config;
import vn.digitalstep.abtask.model.Photo;
import vn.digitalstep.abtask.model.Project;
import vn.digitalstep.abtask.util.PermissionUtil;
import vn.digitalstep.abtask.util.Util;

/**
 * Created by LIO1HC on 10/9/2016.
 */
public class DlgNewProject {
    private Activity mActivity;
    private View mRootView;
    private AlertDialog mDialog;
    private FirebaseUser fUser;
    private DatabaseReference dataRef;
    private OnDlgNewProject listener;
    public ImageView imgClicked;
    public Bitmap[] bitmaps = new Bitmap[4];

    public DlgNewProject(Activity mActivity) {
        this.mActivity = mActivity;
        fUser = FirebaseAuth.getInstance().getCurrentUser();
        dataRef = FirebaseDatabase.getInstance().getReference();
    }

    public interface OnDlgNewProject {
        void onProjectCreated();
    }

    public void setListener(OnDlgNewProject listener) {
        this.listener = listener;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.CustomAlertDialog);
        mRootView = mActivity.getLayoutInflater().inflate(R.layout.dialog_project_info, null);
        builder.setView(mRootView);
        builder.setTitle(R.string.add_project);
        builder.setPositiveButton(R.string.btn_ok, null);
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mDialog.dismiss();
            }
        });

        mDialog = builder.create();
        mDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        Rect displayRectangle = new Rect();
        Window window = mActivity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        WindowManager.LayoutParams wlp = mDialog.getWindow().getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        mDialog.show();
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        mDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

        initView();
    }

    private void submit() {
        String name = ((EditText) mRootView.findViewById(R.id.edt_project_name)).getText().toString();
        String desc = ((EditText) mRootView.findViewById(R.id.edt_project_desc)).getText().toString();

        if (name.equals("")) {
            ((EditText) mRootView.findViewById(R.id.edt_project_name))
                    .setError(mActivity.getString(R.string.empty_info));
            return;
        }
        if (desc.equals("")) {
            ((EditText) mRootView.findViewById(R.id.edt_project_desc))
                    .setError(mActivity.getString(R.string.empty_info));
            return;
        }

        Project project = new Project();
        project.name = name;
        project.description = desc;
        project.timestamp = new Date().getTime();
        project.owner = fUser.getUid();
        String key = dataRef.child(WSConfig.PATH_PROJECTS).child(fUser.getUid()).push().getKey();
        project.key = key;
        dataRef.child(WSConfig.PATH_PROJECTS).child(fUser.getUid()).child(key).setValue(project, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                listener.onProjectCreated();
            }
        });

        //upload vehicle photos
        if (bitmaps[0] != null) {
            uploadPhoto(bitmaps[0], project.key);
        }
        if (bitmaps[1] != null) {
            uploadPhoto(bitmaps[1], project.key);
        }
        if (bitmaps[2] != null) {
            uploadPhoto(bitmaps[2], project.key);
        }
        if (bitmaps[3] != null) {
            uploadPhoto(bitmaps[3], project.key);
        }
    }

    private void uploadPhoto(final Bitmap bitmap, final String key) {
        if (!Util.isOnline(mActivity))
            return;

        Util.showLoading(mActivity);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        final byte[] data = baos.toByteArray();

        final String photoKey = dataRef.child(WSConfig.PATH_PROJECT_PHOTO).child(key).push().getKey();
        StorageReference fileRef = FirebaseStorage.getInstance().getReference().child(WSConfig.STORAGE_PROJECT_PHOTO + "/" + photoKey);
        UploadTask uploadTask = fileRef.putBytes(data);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                //add node to DB
                StorageMetadata metadata = taskSnapshot.getMetadata();
                String url = metadata.getDownloadUrl().toString();
                Photo photo = new Photo();
                photo.url = url;
                photo.key = photoKey;
                photo.timestamp = new Date().getTime();
                dataRef.child(WSConfig.PATH_PROJECT_PHOTO).child(key).child(photoKey).setValue(photo);
                if (bitmap == bitmaps[0]) {
                    dataRef.child(WSConfig.PATH_PROJECTS).child(fUser.getUid()).child(key)
                            .child("urlPhoto").setValue(photo.url);
                }
                Util.hideLoading();
            }
        });
    }

    private void initView() {
        mRootView.findViewById(R.id.img_photo1).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.img_photo2).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.img_photo3).setOnClickListener(onClickListener);
        mRootView.findViewById(R.id.img_photo4).setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_photo1:
                case R.id.img_photo2:
                case R.id.img_photo3:
                case R.id.img_photo4:
                    imgClicked = (ImageView) view;
                    PopupMenu popupMenu = new PopupMenu(mActivity, view);
                    popupMenu.getMenuInflater().inflate(R.menu.popup_menu_image, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(popupItemClickListener);
                    popupMenu.show();
                    break;
                default:
                    break;
            }
        }
    };

    private PopupMenu.OnMenuItemClickListener popupItemClickListener = new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.camera:
                    boolean res = PermissionUtil.getPermission(mActivity, Manifest.permission.CAMERA,
                            PermissionUtil.PERMISSIONS_REQUEST_CAMERA);
                    if (res) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ((MainActivity) mActivity).mCurrentFragment
                                .startActivityForResult(cameraIntent, Config.DLG_NEW_PRJ_CAMERA_REQUEST_CODE);
                    } else {
                        Toast.makeText(mActivity,
                                R.string.denied_permission_dialog_message, Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.gallery:
                    res = true;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        res = PermissionUtil.getPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE,
                                PermissionUtil.PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL);
                    }
                    if (res) {
                        Intent galleryIntent = new Intent();
                        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                        galleryIntent.setType("image/*");
                        ((MainActivity) mActivity).mCurrentFragment
                                .startActivityForResult(galleryIntent, Config.DLG_NEW_PRJ_PICKFILE_REQUEST_CODE);
                    } else {
                        Toast.makeText(mActivity,
                                R.string.denied_permission_dialog_message, Toast.LENGTH_LONG).show();
                    }
                    break;
                default:
                    break;
            }
            return false;
        }
    };
}
